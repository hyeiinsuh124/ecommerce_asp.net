﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="checkout.aspx.cs" Inherits="Hyeiin_Project1.checkout" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Be Healthy Check Out</title>
    <link href="Styles/checkoutCss.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great&display=swap" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">

        <asp:Label ID="lblCheckOut" CssClass="Labels" style="font-size:50px;" runat="server" Text="Check Out"></asp:Label> 

        <iframe id="odInfo" class="MainFrame" src="orderdetail.aspx" runat="server">
        </iframe>

        <iframe id="customerInfo" class="MainFrame" src="customers.aspx" runat="server">
        </iframe>
    </form>
    
</body>
</html>
