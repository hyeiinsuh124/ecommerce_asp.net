﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Sales.aspx.cs" Inherits="Hyeiin_Project1.Sales" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Be Healthy Shop</title>
    <link rel="stylesheet" href="Styles/salesreports.css" />
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great&display=swap" rel="stylesheet" />
</head>
<body>
   <form id="form1" runat="server">
        <asp:Label ID="lblSales" style="font-size: 50px; top: 6px; left: 458px; width: 807px; margin-right: 420px;" CssClass="labels" runat="server" Text="Sales Report"></asp:Label>       
       <br />
        <asp:GridView ID="gridSales" CssClass="grid" runat="server" Style="top: 84px; left: 457px; height: 229px; margin-top: 7px;" AutoGenerateColumns="false">
            <Columns>
                 <asp:BoundField DataField="id" HeaderText="Sales #" ItemStyle-CssClass="Tag1"  HeaderStyle-BackColor="#ffb8a6"/>
                 <asp:BoundField DataField="customername" HeaderText="Customer Name" ItemStyle-CssClass="Tag2" HeaderStyle-BackColor="#ffb8a6" />
                 <asp:BoundField DataField="productcode" HeaderText="Product Code" ItemStyle-CssClass="Tag3" HeaderStyle-BackColor="#ffb8a6"/>
                 <asp:BoundField DataField="description" HeaderText="Description" ItemStyle-CssClass="Tag4" HeaderStyle-BackColor="#ffb8a6" />
                 <asp:BoundField DataField="QtySold" HeaderText="Qty Sold" ItemStyle-CssClass="Tag5" HeaderStyle-BackColor="#ffb8a6"/>
                 <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price"  ItemStyle-CssClass="Tag6" HeaderStyle-BackColor="#ffb8a6"/>
            </Columns>
        </asp:GridView>
       <asp:GridView ID="gridCalc" runat="server" CssClass="grid"  AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="QTYSOLD" HeaderText="Total Sales Quantity"  HeaderStyle-BackColor="#ffb8a6" />
                <asp:BoundField DataField="SELLING" HeaderText="Total Sales Amount"  HeaderStyle-BackColor="#ffb8a6"/>
            </Columns>
        </asp:GridView>
   </form>
</body>
</html>
