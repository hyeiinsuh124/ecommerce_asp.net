﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="promo.aspx.cs" Inherits="Hyeiin_Project1.promo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Be Healthy Promotions</title>
    <link href="Styles/promoStyle.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great&display=swap" rel="stylesheet" />
</head>
<body>
    <form id="form2" runat="server"> 
        <asp:Label ID="lblPromo" runat="server" Text="Our Promotion"></asp:Label>
        <asp:Table ID="Table1" CssClass="tbl" runat="server">

            <asp:TableRow  ID="TableRow1" CssClass="rows" runat="server">
                <asp:TableCell CssClass="imgCell" Width="300px">
                    <img src="Pictures/promo_doga.jpg" alt="daga" />
                </asp:TableCell>
                <asp:TableCell CssClass="PromoTitle">Doga- <br /> Healthy life <br />with your pet!</asp:TableCell>
                <asp:TableCell CssClass="PromoDes">Get the chance to join our doga!<br /><br /> <i>If you subscribe our website, we will give you free trial coupon for our doga!</i></asp:TableCell>
            </asp:TableRow>

             <asp:TableRow  ID="TableRow2" CssClass="rows"  runat="server">
                <asp:TableCell>
                    <img src="Pictures/promo_brekky.jpg" />                  
                </asp:TableCell>
                <asp:TableCell CssClass="PromoTitle">Brekky<br /> Free Delievery<br /> Service</asp:TableCell>
                <asp:TableCell CssClass="PromoDes">Don't Skip your Breakfast anymore! <br /> We will deliever for you to your door!<br /><br /><i>Get 1 month regular breakfast order then delievery is for free at the time you want!</i></asp:TableCell>
            </asp:TableRow>

             <asp:TableRow  ID="TableRow3" CssClass="rows"  runat="server">
                <asp:TableCell>
                    <img src="Pictures/promo_challenge.jpg" />
                </asp:TableCell>
                <asp:TableCell CssClass="PromoTitle">30 Days <br /> Healthy eating Challenge</asp:TableCell>
                <asp:TableCell CssClass="PromoDes">Join our challenges for making healthy habits! <br /><br /><i>Winner will get $1000 coupon for our Healthy shop products</i></asp:TableCell>
            </asp:TableRow>       
        </asp:Table>

        <div id="wrap">
            <a href="www.facebook.com"><img src="Pictures/facebook.png" style="left:638px; margin-top: 0px;" /></a>
            <a href="www.instagram.com"><img src="Pictures/instagram%20(1).png" style="left:711px"/></a>
            <a href="www.linkedin.com"><img src="Pictures/linkedin%20(1).png" style="left:783px"/></a>
            <asp:Label ID="lblInstruction" CssClass="Labels" runat="server" Text="Please contact us about further information"></asp:Label>
        </div>
    </form>
   
</body>
</html>
