﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Hyeiin_Project1._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Be Healthy Shop</title>
    <link href="Styles/default.css" rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great&display=swap" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="MainPanel" runat="server" CssClass="MainPanel" style="right: 250px">
            <asp:Label ID="lblShopName" CssClass="Labels" style="font-size: 60px; top: 4px; left: -25px; width: 259px; height: 100px; margin-left: 0px; text-align:right; color: #808080" runat="server" Text="Be Healthy Shop"></asp:Label>
            <asp:Button ID="btnCatalog" CssClass="Buttons"  style="top: 300px; left: 52px;" runat="server" Text="Catalog" OnClick="btnCatalog_Click" />
            <asp:Button ID="btnCart" CssClass="Buttons" style="top: 378px; left: 52px;" runat="server" Text="Cart" OnClick="btnCart_Click" />
            <asp:Button ID="btnPromotions" CssClass="Buttons" style="top: 461px; left: 52px;" runat="server" Text="Promotions" OnClick="btnPromotions_Click" />
            <asp:Button ID="btnCustomers" CssClass="Buttons" style="top: 541px; left: 51px" runat="server" Text="Customers" OnClick="btnCustomers_Click" />
            <asp:Button ID="btnProducts"  CssClass="Buttons" style="top: 615px; left: 52px" runat="server" Text="Products" OnClick="btnProducts_Click" />
            <asp:Button ID="btnReports"  CssClass="Buttons" style="top: 681px; left: 53px" runat="server" Text="Reports" OnClick="btnReports_Click" />
        </asp:Panel>
        <iframe id="contentFrame" class="MainFrame" src="" runat="server">
        </iframe>
    </form>
</body>
</html>
