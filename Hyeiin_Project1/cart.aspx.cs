﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hyeiin_Project1
{
    public partial class cart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnRecalc.Click += new EventHandler(btnRecalc_Click);

            cartGrid();
            recalculate();
        }

        protected void btnTemplateRemove_Click(object sender, EventArgs e)
        {
            Button removeB = (Button)sender;
            string id = removeB.ID;

            string[] idParts = id.Split('_');

            int rowNum = int.Parse(idParts[1]);
            lblSelectedRow.Visible = true;
            lblSelectedRow.Text = "You selected row " + rowNum;

            _default.qty[_default.cartInfo[rowNum]] = "1";

            for (int i = rowNum; i < _default.numItems; i++)
            {
                _default.cartInfo[i] = _default.cartInfo[i + 1];
            }

            _default.numItems--;

            cartGrid();
            recalculate();
        }

        protected void btnRecalc_Click(object sender, EventArgs e)
        {
            recalculate();
        }

        private void cartGrid()
        {
            tblCart.Rows.Clear();

            for(int i = 0; i < _default.numItems; i++)
            {
                TableRow row = new TableRow();

                for(int j = 0; j < 5; j++)
                {
                    TableCell cell = new TableCell();

                    if(j == 0)
                    {
                        Image image = new Image();
                        image.ImageUrl = _default.pictures[_default.cartInfo[i]];
                        image.Height = 200;
                        image.Width = 150;
                        cell.Controls.Add(image);
                    }
                    else if(j == 1)
                    {
                        Label label = new Label();
                        label.Text = _default.pName[_default.cartInfo[i]];
                        label.Width = 150;
                        cell.Controls.Add(label);
                    }
                    else if(j == 2)
                    {
                        Label label = new Label();
                        label.Text = _default.price[_default.cartInfo[i]];                    
                        cell.Width = 50;
                        cell.Controls.Add(label);
                    }
                    else if(j == 3)
                    {
                        TextBox text = new TextBox();
                        text.Text = _default.qty[_default.cartInfo[i]];
                        text.Width = 50;
                        cell.Controls.Add(text);
                    }
                    else if(j == 4)
                    {
                        Button btnRemoveFromCart = new Button();
                        btnRemoveFromCart.ID = "btnSelect_" + i + "_" + j;
                        btnRemoveFromCart.Click += new EventHandler(btnTemplateRemove_Click);
                        btnRemoveFromCart.Text = "Remove";
                        btnRemoveFromCart.CssClass = "removeBtn";
                        
                        cell.Controls.Add(btnRemoveFromCart);
                    }
                    row.Cells.Add(cell);
                }
                tblCart.Rows.Add(row);
            }
        }
        private void recalculate()
        {
            decimal total = 0;

            for (int i = 0; i < _default.numItems; i++)
            {
                TableRow row = tblCart.Rows[i];
                decimal rowPrice = 0;

                for (int j = 0; j < 5; j++)
                {
                    TableCell cell = row.Cells[j];

                    if (j == 2)
                    {
                        Control ctrl = cell.Controls[0];
                        Label lbl = (Label)ctrl;
                        string price = lbl.Text;
                        rowPrice = decimal.Parse(price);
                    }
                    else if (j == 3)
                    {
                        Control ctrl = cell.Controls[0];
                        TextBox txt = (TextBox)ctrl;
                        string qty = txt.Text;
                        _default.qty[_default.cartInfo[i]] = qty;
                        decimal rowTotal = rowPrice * int.Parse(qty);
                        total += rowTotal;
                    }
                }
                lblTotal.Text = total.ToString("$##,##0.##");
            }
             
        }
        protected void btnCheckOut_Click(object sender, EventArgs e)
        {
            Server.Transfer("checkout.aspx");
        }
    }
}