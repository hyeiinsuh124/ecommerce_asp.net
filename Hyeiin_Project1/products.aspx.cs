﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace Hyeiin_Project1
{
    public partial class products : System.Web.UI.Page
    {
        string dbConnect = @"integrated security=True; data source=(localdb)\ProjectsV13;persist security info=False;initial catalog=eStore";
        string importPicture = HttpContext.Current.Server.MapPath(".") + @"\Image";
        //string webData = HttpContext.Current.Server.MapPath(".") + @"\Data\Products\";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string[] productPictures = Directory.GetFiles(importPicture, "*.*");
                dropProductPhoto.Items.Clear();

                dropProductPhoto.Items.Insert(0, new ListItem("", ""));
                dropProductPhoto.SelectedIndex = 0;


                for (int i = 0; i < productPictures.Length; i++)
                {
                    string picName = Path.GetFileName(productPictures[i]);
                    dropProductPhoto.Items.Add(picName);
                }
            }
        }

        protected void dropProductPhoto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dropProductPhoto.SelectedIndex > 0)
            {
                imgContainer.ImageUrl = "Image/" + dropProductPhoto.SelectedItem;
                txtPic.Text = dropProductPhoto.SelectedItem.ToString();
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            //dataset
            SqlDataAdapter dataAdapter = null;
            DataSet ds = null;

            SqlConnection connectCmd = new SqlConnection(dbConnect);
            SqlCommand cmd = null;
            connectCmd.Open();//available to access to table in dbConnect path

            //query will be different -whate ever in query u need to use it as a parameter in try
            string query = "SELECT * FROM Products WHERE ProductId = @pid";

            try
            {
                ds = new DataSet();
                cmd = new SqlCommand(query, connectCmd);

                cmd.Parameters.AddWithValue("@pid", txtProductId.Text);

                //create a new sql adapter to retrieve data and store it into a dataset
                dataAdapter = new SqlDataAdapter();
                //when we perform the select query, the function we use is Selectcommand from datadapter
                //all the data that retrieved from select query will be store in cmd and then add to the dataset
                dataAdapter.SelectCommand = cmd;
                //dataset is equivalent to the table in ds(add cmd to ds from products table
                //below ds will have all the values from select query (oh!)
                //ds is that data
                dataAdapter.Fill(ds, "Products");

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                DisposeResources(ref connectCmd, ref cmd, ref dataAdapter, ref ds);
                return;
            }

            if (ds.Tables["Products"].Rows.Count == 1)
            {
                txtProductCode.Text = ds.Tables["Products"].Rows[0]["ProductCode"].ToString();
                txtProductName.Text = ds.Tables["Products"].Rows[0]["ProductName"].ToString();
                txtDescrip.Text = ds.Tables["Products"].Rows[0]["Description"].ToString();
                txtPic.Text = ds.Tables["Products"].Rows[0]["Picture"].ToString();
                txtQuantity.Text = ds.Tables["Products"].Rows[0]["Quantity"].ToString();
                txtPrice.Text = ds.Tables["Products"].Rows[0]["Price"].ToString();

                imgContainer.ImageUrl = "image/" + txtPic.Text;

                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
            }
            else
            {
                displayMessage();
                lblMessage.Text = "Product Not Found";
            }
            DisposeResources(ref connectCmd, ref cmd, ref dataAdapter, ref ds);
        }
        private void displayMessage()
        {
            lblMessage.Visible = true;
        }
        private void hide()
        {
            lblMessage.Visible = false;
            btnVerify.Visible = false;
        }
        protected void btnNew_Click(object sender, EventArgs e)
        {
            resetInputField();
            hide();
        }
        private void resetInputField()
        {
            txtProductId.Text = ""; 
            txtProductCode.Text = "";
            txtProductName.Text = "";
            txtDescrip.Text = "";
            txtPic.Text = "";
            txtQuantity.Text = "";
            txtPrice.Text = "";
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            SqlConnection connectCmd = null;
            SqlCommand cmd = null;

           
            connectCmd = new SqlConnection(dbConnect);
            connectCmd.Open();
            string query = "INSERT INTO Products(ProductCode, ProductName ,Description, Picture, Quantity, Price) Values(@pc, @pn, @des, @pic, @qty, @pr)";

            try
            {
                cmd = new SqlCommand(query, connectCmd);
                cmd.Parameters.AddWithValue("@pc", txtProductCode.Text);
                cmd.Parameters.AddWithValue("@pn", txtProductName.Text);
                cmd.Parameters.AddWithValue("@des", txtDescrip.Text);
                cmd.Parameters.AddWithValue("@pic", txtPic.Text);
                cmd.Parameters.AddWithValue("@qty", txtQuantity.Text);
                cmd.Parameters.AddWithValue("@pr", txtPrice.Text);

                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                lblMessage.Text = ex.Message;
                return;
            }

            String id = "SELECT IDENT_CURRENT('Products') FROM Products";
            cmd = new SqlCommand(id, connectCmd);
            //RETURN VALUE THE SINGLE ITEM(ExecuteScalar)
            int idValue = Convert.ToInt32(cmd.ExecuteScalar());
            txtProductId.Text = idValue.ToString();

            lblMessage.Text = "New Product Added!";

            displayMessage();
            
            DisposeResources(ref connectCmd, ref cmd);

            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //1. find the id first 
            //Step 1: Opening connection to the database
            SqlConnection connectCmd = new SqlConnection(dbConnect);
            SqlCommand cmd = null;
            connectCmd.Open();//available to access to table in dbConnect path

            //query will be different
            string query = "UPDATE Products SET ProductCode=@pc, ProductName=@pn, Description=@des, Picture=@pic, Quantity=@qty, Price=@pr WHERE ProductId=@pid";
            //Step2: Perform CRUD operation in this case
            try
            {
                cmd = new SqlCommand(query, connectCmd);
                cmd.Parameters.AddWithValue("@pid", txtProductId.Text);
                cmd.Parameters.AddWithValue("@pc", txtProductCode.Text);
                cmd.Parameters.AddWithValue("@pn", txtProductName.Text);
                cmd.Parameters.AddWithValue("@des", txtDescrip.Text);
                cmd.Parameters.AddWithValue("@pic", txtPic.Text);
                cmd.Parameters.AddWithValue("@qty", txtQuantity.Text);
                cmd.Parameters.AddWithValue("@pr", txtPrice.Text);

                //INSERT, UPDATE, DELETE QUERY
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                DisposeResources(ref connectCmd, ref cmd);
                return;
            }

            lblMessage.Text = "Product info Updated";

            displayMessage();
            DisposeResources(ref connectCmd, ref cmd);
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            btnVerify.Visible = true;
        }

        protected void btnVerify_Click(object sender, EventArgs e)
        {
            deleteProduct();
        }
        private void deleteProduct()
        {
            //1. find the id first 
            //Step 1: Opening connection to the database
            SqlConnection connectCmd = new SqlConnection(dbConnect);
            SqlCommand cmd = null;
            connectCmd.Open();//available to access to table in dbConnect path

            //query will be different -whate ever in query u need to use it as a parameter in try
            string query = "DELETE FROM Products WHERE ProductId=@pid";
            //Step2: Perform CRUD operation in this case
            try
            {
                cmd = new SqlCommand(query, connectCmd);
                cmd.Parameters.AddWithValue("@pid", txtProductId.Text);

                //INSERT, UPDATE, DELETE QUERY
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                DisposeResources(ref connectCmd, ref cmd);
                return;
            }

            lblMessage.Text = "Product info Deleted";

            resetInputField();
            displayMessage();
            DisposeResources(ref connectCmd, ref cmd);

            btnVerify.Visible = false;
        }

        private static void DisposeResources(ref SqlConnection connectCmd, ref SqlCommand cmd)
        {
            if (connectCmd != null)
            {
                connectCmd.Dispose();
            }
            if (cmd != null)
            {
                cmd.Dispose();
            }
        }

        private static void DisposeResources(ref SqlConnection connectCmd, ref SqlCommand cmd, ref SqlDataAdapter dataAdapter, ref DataSet ds)
        {
            if (connectCmd != null)
            {
                connectCmd.Dispose();
            }
            if (cmd != null)
            {
                cmd.Dispose();
            }

            if (dataAdapter != null)
            {
                dataAdapter.Dispose();
            }
            if (ds != null)
            {
                ds.Dispose();
            }

        }
    }
}