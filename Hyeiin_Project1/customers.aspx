﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="customers.aspx.cs" Inherits="Hyeiin_Project1.customers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Be Healthy Customers</title>
    <link rel="stylesheet" href="Styles/custCss.css" />
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great&display=swap" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblTitle" CssClass="Labels" style="top:50px; font-size: 50px;" runat="server" Text="Our Customers"></asp:Label>

        <asp:Label ID="lblCustId"  CssClass="Labels" style="top: 171px" runat="server"  Text="Customer #"></asp:Label>
        <asp:TextBox ID="txtCustId" CssClass="TextBoxs" style="top: 172px; width: 68px;"  runat="server"></asp:TextBox>

        <asp:Label ID="lblCustName"  CssClass="Labels" style="top: 219px" runat="server" Text="Customer Name"></asp:Label>
        <asp:TextBox ID="txtCustName" CssClass="TextBoxs" style="top: 220px; margin-bottom: 0px;"  runat="server"></asp:TextBox>

        <asp:Label ID="lblCustAddress"  CssClass="Labels" style="top: 267px" runat="server" Text="Street Address"></asp:Label>
        <asp:TextBox ID="txtCustAddress" CssClass="TextBoxs"  style="top: 268px; width: 257px;"  runat="server"></asp:TextBox>

        <asp:Label ID="lblCustCity" CssClass="Labels" style="top: 315px" runat="server" Text="City"></asp:Label>
        <asp:TextBox ID="txtCustCity" CssClass="TextBoxs" style="top: 318px; width: 155px;"  runat="server"></asp:TextBox>

        <asp:Label ID="lblCustProvince" CssClass="Labels" style="top: 366px" runat="server" Text="Province"></asp:Label>
        <asp:TextBox ID="txtProvince" CssClass="TextBoxs" style="top: 367px; width: 158px;"  runat="server"></asp:TextBox>

        <asp:Label ID="lblPostCode" CssClass="Labels" style="top: 414px" runat="server" Text="Postal Code"></asp:Label>
        <asp:TextBox ID="txtPostCode" CssClass="TextBoxs" style="top: 418px; width: 158px;"  runat="server"></asp:TextBox>

        <asp:Label ID="lblEmail" CssClass="Labels" style="top: 466px" runat="server" Text="Email"></asp:Label>
        <asp:TextBox ID="txtEmail" CssClass="TextBoxs" style="top: 466px; width: 197px;"  runat="server"></asp:TextBox>

        <asp:Label ID="lblCustPhone" CssClass="Labels" style="top: 518px; height: 22px;" runat="server" Text="Phone #"></asp:Label>
        <asp:TextBox ID="txtCustPhone" CssClass="TextBoxs" style="top: 519px; width: 199px;" runat="server"></asp:TextBox>
        
        <asp:Label ID="lblMessage" CssClass="Labels" style="top: 567px; left: 350px; bottom: 51px; color: #ffc0cb" Visible="false" runat="server" Text=""></asp:Label>
        <asp:Button ID="btnFind" CssClass="Buttons" style="left: 348px" runat="server" Text="Search"  OnClick="btnFind_Click" /> 
        <asp:Button ID="btnNew" CssClass="Buttons" style="left: 469px" runat="server" Text="New" OnClick="btnNew_Click" />
        <asp:Button ID="btnAdd" CssClass="Buttons" style="left: 593px" runat="server" Text="Add" OnClick="btnAdd_Click" />
        <asp:Button ID="btnUpdate" CssClass="Buttons"  style="left: 717px" Enabled="false" runat="server" Text="Update" OnClick="btnUpdate_Click" />
        <asp:Button ID="btnDelete" CssClass="Buttons" style="left: 841px; margin-top: 1px;" Enabled="false" runat="server" Text="Delete" OnClick="btnDelete_Click" />
        <asp:Button ID="btnVerify"  CssClass="Buttons" style="left: 839px; width: 178px;" Visible="false" runat="server" Text="Are you sure to delete?" OnClick="btnVerify_Click"/>
        
    </form>
</body>
</html>