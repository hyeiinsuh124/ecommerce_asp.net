﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hyeiin_Project1
{
    public partial class orderdetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BtnToPay.Click += new EventHandler(BtnToPay_Click);
            BtnToBack.Click += new EventHandler(BtnToBack_Click);

            CreateDetail();
            CalcTotal();
        }

        private void CreateDetail()
        {
            tblDetail.Rows.Clear();

            for(int i = 0; i < _default.numItems; i++)
            {
                TableRow row = new TableRow();
               
                for(int j = 0; j < 4; j++)
                {
                    TableCell cell = new TableCell();

                    if (j == 0)
                    {
                        Label label = new Label();
                        label.Text = _default.pName[_default.cartInfo[i]];
                        label.Width = 200;
                        cell.Controls.Add(label);
                    }
                    else if(j == 1)
                    {
                        Label label = new Label();
                        decimal price = Decimal.Parse(_default.price[_default.cartInfo[i]]);
                        label.Text = price.ToString("$##,##0.#0");
                        label.Width = 100;
                        cell.Controls.Add(label);
                    }
                    else if(j == 2)
                    {
                        TextBox text = new TextBox();
                        text.Text = _default.qty[_default.cartInfo[i]];
                        text.Enabled = false;
                        text.Width = 25;
                        cell.Width = 50;

                        cell.Controls.Add(text);
                    }
                    else if (j == 3)
                    {
                        Label lbl = new Label();

                        decimal rowTotal = Decimal.Parse(_default.price[_default.cartInfo[i]]) * int.Parse(_default.qty[_default.cartInfo[i]]);

                        lbl.Text =  "Product Total : " + rowTotal.ToString("$##,##0.#0");
                        lbl.Width = 200;

                        cell.Controls.Add(lbl);
                    }
                    row.Cells.Add(cell);
                }
                tblDetail.Rows.Add(row);
            }
        }

        private void CalcTotal()
        {
            decimal total = 0;

            for(int i = 0; i <_default.numItems; i++)
            {
                TableRow row = tblDetail.Rows[i];
                decimal rowPrice = Decimal.Parse(_default.price[_default.cartInfo[i]]);

                for (int j = 0; j < 4; j++)
                {
                    TableCell cell = row.Cells[j];

                    if(j == 2)
                    {
                        Control ctrl = cell.Controls[0];
                        TextBox txt = (TextBox)ctrl;
                        string qty = txt.Text;
                        _default.qty[_default.cartInfo[i]] = qty;

                        decimal rowTotal = rowPrice * int.Parse(qty);
                        total += rowTotal;
                    }
                }
            }
            lblTotal.Text = total.ToString("$##,##0.#0");
        }

        protected void BtnToPay_Click(object sender, EventArgs e)
        {
            string dbConnect = @"integrated security=True; data source=(localdb)\ProjectsV13;persist security info=False;initial catalog=eStore";

            SqlConnection connectCmd = null;
            SqlCommand cmd = null;

            connectCmd = new SqlConnection(dbConnect);

            connectCmd.Open();

            string query = "INSERT INTO Sales(ProductId, custId, QtySold, SellingPrice, OrderDate) VALUES(@pid, @cid, @qty, @price, @od)";

            try
            {
                for(int i = 0; i < _default.numItems; i++) { 
                    cmd = new SqlCommand(query, connectCmd);
                    cmd.Parameters.AddWithValue("@pid", _default.productId[_default.cartInfo[i]]);
                    cmd.Parameters.AddWithValue("@cid", _default.customerId);
                    cmd.Parameters.AddWithValue("@qty", _default.qty[_default.cartInfo[i]]);
                    cmd.Parameters.AddWithValue("@price", _default.price[_default.cartInfo[i]]);
                    cmd.Parameters.AddWithValue("@od", DateTime.Now.ToString("yyyy-MM-dd"));
               
                    //INSERT, UPDATE, DELETE QUERY
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                DisposeResources(ref connectCmd, ref cmd);
                return;
            }

        }

        protected void BtnToBack_Click(object sender, EventArgs e)
        {
            //code later in next half term
        }

        private static void DisposeResources(ref SqlConnection connectCmd, ref SqlCommand cmd)
        {
            if (connectCmd != null)
            {
                connectCmd.Dispose();
            }
            if (cmd != null)
            {
                cmd.Dispose();
            }
        }
    }
}