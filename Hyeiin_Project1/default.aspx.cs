﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hyeiin_Project1
{
    public partial class _default : System.Web.UI.Page
    {
        public static string webSiteData = HttpContext.Current.Server.MapPath(".") + @"\Data\Products";
        //public static string webCustData = HttpContext.Current.Server.MapPath(".") + @"\Data\Customers";

        
        public static string[] pictures;
        public static string[] pName;
        public static string[] description;
        public static string[] price;

        public static int[] cartInfo = new int[100];
        public static int numItems = 0;
        public static string[] qty = new string[100];

        public static int customerId;
        public static int[] productId;

        //public static string countCustomers;
        //public static string custId;
        //public static string custName;
        //public static string custAdd;
        //public static string custCity;
        //public static string custProvince;
        //public static string custPostal;
        //public static string custemail;
        //public static string custphone;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetArrays();
               
                for(int i = 0; i < qty.Length; i++)
                {
                    qty[i] = "1";
                }
            }
        }

        protected void btnCatalog_Click(object sender, EventArgs e)
        {
            contentFrame.Attributes.Add("src", "catalog.aspx");
        }

        protected void btnCart_Click(object sender, EventArgs e)
        {
            contentFrame.Attributes.Add("src", "cart.aspx");
        }

        protected void btnPromotions_Click(object sender, EventArgs e)
        {
            contentFrame.Attributes.Add("src", "promo.aspx");
        }

        protected void btnCustomers_Click(object sender, EventArgs e)
        {
           contentFrame.Attributes.Add("src", "customers.aspx");
        }

        protected void btnProducts_Click(object sender, EventArgs e)
        {
            contentFrame.Attributes.Add("src", "products.aspx");

        }

        public static void resetArrays()
        {
            //countFiles = Directory.GetFiles(webSiteData, "*.*");
            string dbConnect = @"integrated security=True; data source=(localdb)\ProjectsV13;persist security info=False;initial catalog=eStore";

            SqlConnection connectCmd = null;
            SqlCommand cmd = null;


            connectCmd = new SqlConnection(dbConnect);
            connectCmd.Open();

            string query = "SELECT COUNT(*) FROM Products";

            cmd = new SqlCommand(query, connectCmd);

            //RETURN VALUE THE SINGLE ITEM(ExecuteScalar)
            int countRows = Convert.ToInt32(cmd.ExecuteScalar());

            pictures = new string[countRows];
            description = new string[countRows];
            pName = new string[countRows];
            price = new string[countRows];

            customerId = 0;
            productId = new int[countRows];
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            contentFrame.Attributes.Add("src", "Sales.aspx");
        }
    }
}