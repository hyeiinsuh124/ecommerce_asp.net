﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hyeiin_Project1
{
    public partial class catalog : System.Web.UI.Page
    {
        string dbConnect = @"integrated security=True; data source=(localdb)\ProjectsV13;persist security info=False;initial catalog=eStore";

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataAdapter dataAdapter = null;
            DataSet ds = null;

            SqlConnection connectCmd = new SqlConnection(dbConnect);
            SqlCommand cmd = null;
            connectCmd.Open();//available to access to table in dbConnect path

            _default.resetArrays();

            //query will be different -whate ever in query u need to use it as a parameter in try
            string query = "SELECT * FROM Products";

            try
            {
                ds = new DataSet();
                cmd = new SqlCommand(query, connectCmd);

                //cmd.Parameters.AddWithValue("@pid", txtProductId.Text);

                //create a new sql adapter to retrieve data and store it into a dataset
                dataAdapter = new SqlDataAdapter();
                //when we perform the select query, the function we use is Selectcommand from datadapter
                //all the data that retrieved from select query will be store in cmd and then add to the dataset
                dataAdapter.SelectCommand = cmd;
                //dataset is equivalent to the table in ds(add cmd to ds from products table
                //below ds will have all the values from select query (oh!)
                //ds is that data
                dataAdapter.Fill(ds, "Products");

            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                DisposeResources(ref connectCmd, ref cmd, ref dataAdapter, ref ds);
                return;
            }

           //iterate through all the products in dataset and display in tale format
           if(ds.Tables["Products"].Rows.Count > 0)
            {
                
                for (int i = 0; i < ds.Tables["Products"].Rows.Count; i++)
                {
                    TableRow row = new TableRow();

                    for (int j = 0; j < 5; j++)
                    {
                        TableCell cell = new TableCell();
                        if (j == 0)
                        {
                           
                            Image img = new Image();
                            img.ImageUrl = "Image/" + ds.Tables["Products"].Rows[i]["Picture"].ToString();
                            img.Height = 200;
                            img.Width = 150;

                            _default.pictures[i] = img.ImageUrl;
                            _default.productId[i] = Convert.ToInt32(ds.Tables["Products"].Rows[i]["ProductId"]);
                            cell.HorizontalAlign = HorizontalAlign.Center;
                            cell.Controls.Add(img);

                           
                        }
                        else if (j == 1)
                        {
                            Label label = new Label();
                            label.Text =ds.Tables["Products"].Rows[i]["ProductName"].ToString();
                            label.CssClass = "lblpName";

                            _default.pName[i] = label.Text;

                            cell.Height = 30;
                            cell.HorizontalAlign = HorizontalAlign.Center;
                            cell.Controls.Add(label);
                        }
                        else if (j == 2)
                        {
                            Label label = new Label();

                            label.Text = ds.Tables["Products"].Rows[i]["Description"].ToString();

                            _default.description[i] = label.Text;

                            cell.Height = 80;
                            cell.HorizontalAlign = HorizontalAlign.Center;
                            cell.Controls.Add(label);
                        }
                        else if (j == 3)
                        {
                            Label label = new Label();
                            label.Text = ds.Tables["Products"].Rows[i]["Price"].ToString();

                            _default.price[i] = label.Text;

                            cell.HorizontalAlign = HorizontalAlign.Center;

                            cell.Controls.Add(label);
                            cell.Height = 30;
                        }
                        else if (j == 4)
                        {
                            Button btnToCart = new Button();
                            btnToCart.ID = "btnSelect_" + i + "_" + j;//btnSelect_0_1
                            btnToCart.CssClass = "Buttons";

                            cell.HorizontalAlign = HorizontalAlign.Center;
                            btnToCart.Click += new EventHandler(btnTemplate_Click);
                            btnToCart.Text = "Add to Cart";
                            cell.Controls.Add(btnToCart);
                        }
                        row.Cells.Add(cell);
                    }
                    tblCatalog.Rows.Add(row);

                }
            }

            DisposeResources(ref connectCmd, ref cmd, ref dataAdapter, ref ds);
        }

        protected void btnTemplate_Click(object sender, EventArgs e)
        {
            Button btnSelect = (Button)sender;

            string id = btnSelect.ID;

            btnSelect.Style["background-color"] = "lightgray";

            string[] btnId = id.Split('_');

            int rowNum = int.Parse(btnId[1]);
            lblSelected.Text = "You Selected " + _default.pName[rowNum];

            if (_default.numItems > 0)
            {
                bool matchRow = false;

                for (int i = 0; i < _default.numItems; i++)
                {
                    if (rowNum == _default.cartInfo[i])
                    {
                        matchRow = true;
                    }
                }

                if (!matchRow)
                {
                    _default.cartInfo[_default.numItems] = rowNum;
                    _default.numItems++;
                }
            }
            else
            {
                _default.cartInfo[_default.numItems] = rowNum;
                _default.numItems++;
            }
        }

        private static void DisposeResources(ref SqlConnection connectCmd, ref SqlCommand cmd)
        {
            if (connectCmd != null)
            {
                connectCmd.Dispose();
            }
            if (cmd != null)
            {
                cmd.Dispose();
            }
        }

        private static void DisposeResources(ref SqlConnection connectCmd, ref SqlCommand cmd, ref SqlDataAdapter dataAdapter, ref DataSet ds)
        {
            if (connectCmd != null)
            {
                connectCmd.Dispose();
            }
            if (cmd != null)
            {
                cmd.Dispose();
            }

            if (dataAdapter != null)
            {
                dataAdapter.Dispose();
            }
            if (ds != null)
            {
                ds.Dispose();
            }

        }
    }
}