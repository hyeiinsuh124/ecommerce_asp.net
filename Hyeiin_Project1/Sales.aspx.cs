﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hyeiin_Project1
{
    public partial class Sales : System.Web.UI.Page
    {
        string dbConnect = @"integrated security=True; data source=(localdb)\ProjectsV13;persist security info=False;initial catalog=eStore";
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataAdapter dataAdapter = null;
            DataSet ds = null;

            SqlConnection connectCmd = new SqlConnection(dbConnect);
            SqlCommand cmd = null;
            connectCmd.Open();

            string query = "SELECT s.Id, c.customername, p.productcode, p.description, s.QtySold, s.SellingPrice FROM customers c INNER JOIN sales s ON c.customer# = s.CustId INNER JOIN products p ON s.ProductId = p.ProductId";
            string query2 = "SELECT SUM(QtySold) AS 'QTYSOLD', SUM(QTYSOLD * SellingPrice) AS 'SELLING' FROM SALES";
            try
            {
                //gridSalesReport grid
                ds = new DataSet();
                cmd = new SqlCommand(query, connectCmd);

                dataAdapter = new SqlDataAdapter();

                dataAdapter.SelectCommand = cmd;

                DataTable dtbl = new DataTable();

                dataAdapter.Fill(dtbl);

                gridSales.DataSource = dtbl;
                gridSales.DataBind();
                
                //gridCalc grid
                ds = new DataSet();
                cmd = new SqlCommand(query2, connectCmd);

                dataAdapter = new SqlDataAdapter();

                dataAdapter.SelectCommand = cmd;
                DataTable dtbl2 = new DataTable();
                dataAdapter.Fill(dtbl2);

                gridCalc.DataSource = dtbl2;
                gridCalc.DataBind();

            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                DisposeResources(ref connectCmd, ref cmd, ref dataAdapter, ref ds);
                return;
            }
          
        }

        private static void DisposeResources(ref SqlConnection connectCmd, ref SqlCommand cmd)
        {
            if (connectCmd != null)
            {
                connectCmd.Dispose();
            }
            if (cmd != null)
            {
                cmd.Dispose();
            }
        }

        private static void DisposeResources(ref SqlConnection connectCmd, ref SqlCommand cmd, ref SqlDataAdapter dataAdapter, ref DataSet ds)
        {
            if (connectCmd != null)
            {
                connectCmd.Dispose();
            }
            if (cmd != null)
            {
                cmd.Dispose();
            }

            if (dataAdapter != null)
            {
                dataAdapter.Dispose();
            }
            if (ds != null)
            {
                ds.Dispose();
            }

        }
    }
}