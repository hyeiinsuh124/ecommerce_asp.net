﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Hyeiin_Project1
{
    public partial class customers : System.Web.UI.Page
    {
        string dbConnect = @"integrated security=True; data source=(localdb)\ProjectsV13;persist security info=False;initial catalog=eStore";
      
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
    
        protected void btnAdd_Click(object sender, EventArgs e)
        {

            //Opening connection to the database
            SqlConnection connectCmd = new SqlConnection(dbConnect);
            SqlCommand cmd = null;
            connectCmd.Open();//available to access to table in dbConnect path

            string query = "INSERT INTO Customers(CustomerName, Address, City, Province, PostalCode, Email, PhoneNumber) VALUES(@name, @add, @ct, @pv, @pc, @em, @ph)";
            //Step2: Perform CRUD operation in this case
            try
            {
                cmd = new SqlCommand(query, connectCmd);
                cmd.Parameters.AddWithValue("@name", txtCustName.Text);
                cmd.Parameters.AddWithValue("@add", txtCustAddress.Text);
                cmd.Parameters.AddWithValue("@ct", txtCustCity.Text);
                cmd.Parameters.AddWithValue("@pv", txtProvince.Text);
                cmd.Parameters.AddWithValue("@pc", txtPostCode.Text);
                cmd.Parameters.AddWithValue("@em", txtEmail.Text);
                cmd.Parameters.AddWithValue("@ph", txtCustPhone.Text);
                //INSERT, UPDATE, DELETE QUERY
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                lblMessage.Text = ex.Message;
                DisposeResources(ref connectCmd, ref cmd);
                return;
            }

            String id = "SELECT IDENT_CURRENT('Customers') FROM Customers";
            cmd = new SqlCommand(id, connectCmd);
            //RETURN VALUE THE SINGLE ITEM(ExecuteScalar)
            int idValue = Convert.ToInt32(cmd.ExecuteScalar());
            txtCustId.Text = idValue.ToString();

            lblMessage.Text = "New Customer Added!";

            _default.customerId = idValue;
            displayMessage();
            DisposeResources(ref connectCmd, ref cmd);

            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;
        }

        private void resetTextField()
        {
            txtCustId.Text = "";
            txtCustName.Text = "";
            txtCustAddress.Text = "";
            txtCustCity.Text = "";
            txtProvince.Text = "";
            txtPostCode.Text = "";
            txtEmail.Text = "";
            txtCustPhone.Text = "";
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //1. find the id first 
            //Step 1: Opening connection to the database
            SqlConnection connectCmd = new SqlConnection(dbConnect);
            SqlCommand cmd = null;
            connectCmd.Open();//available to access to table in dbConnect path
            
            //query will be different
            string query = "UPDATE Customers SET CustomerName=@name, Address=@add, City=@ct, Province=@pv, PostalCode=@pc, Email=@em, PhoneNumber=@ph WHERE Customer#=@cid";
            //Step2: Perform CRUD operation in this case
            try
            {
                cmd = new SqlCommand(query, connectCmd);
                cmd.Parameters.AddWithValue("@cid", txtCustId.Text);
                cmd.Parameters.AddWithValue("@name", txtCustName.Text);
                cmd.Parameters.AddWithValue("@add", txtCustAddress.Text);
                cmd.Parameters.AddWithValue("@ct", txtCustCity.Text);
                cmd.Parameters.AddWithValue("@pv", txtProvince.Text);
                cmd.Parameters.AddWithValue("@pc", txtPostCode.Text);
                cmd.Parameters.AddWithValue("@em", txtEmail.Text);
                cmd.Parameters.AddWithValue("@ph", txtCustPhone.Text);
                //INSERT, UPDATE, DELETE QUERY
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                DisposeResources(ref connectCmd, ref cmd);
                return;
            }

            lblMessage.Text = "Customer info Updated";

            displayMessage();
            DisposeResources(ref connectCmd, ref cmd);
        }

        private void deleteCust()
        {
            //1. find the id first 
            //Step 1: Opening connection to the database
            SqlConnection connectCmd = new SqlConnection(dbConnect);
            SqlCommand cmd = null;
            connectCmd.Open();//available to access to table in dbConnect path

            //query will be different -whate ever in query u need to use it as a parameter in try
            string query = "DELETE FROM Customers WHERE Customer#=@cid";
            //Step2: Perform CRUD operation in this case
            try
            {
                cmd = new SqlCommand(query, connectCmd);
                cmd.Parameters.AddWithValue("@cid", txtCustId.Text);
             
                //INSERT, UPDATE, DELETE QUERY
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                DisposeResources(ref connectCmd, ref cmd);
                return;
            }

            lblMessage.Text = "Customer info Deleted";

            resetTextField();
            displayMessage();
            DisposeResources(ref connectCmd, ref cmd);
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            btnVerify.Visible = true;
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            //dataset
            SqlDataAdapter dataAdapter = null;
            DataSet ds = null;

            SqlConnection connectCmd = new SqlConnection(dbConnect);
            SqlCommand cmd = null;
            connectCmd.Open();//available to access to table in dbConnect path

            //query will be different -whate ever in query u need to use it as a parameter in try
            string query = "SELECT * FROM Customers WHERE Customer# = @cid";
           
            try
            {
                ds = new DataSet();
                cmd = new SqlCommand(query, connectCmd);

                cmd.Parameters.AddWithValue("@cid", txtCustId.Text);

                //create a new sql adapter to retrieve data and store it into a dataset
                dataAdapter = new SqlDataAdapter();
                //when we perform the select query, the function we use is Selectcommand from datadapter
                //all the data that retrieved from select query will be store in cmd and then add to the dataset
                dataAdapter.SelectCommand = cmd;
                //dataset is equivalent to the table in ds(add cmd to ds from customers table
                //below ds will have all the values from select query (oh!)
                //ds is that data
                dataAdapter.Fill(ds, "Customers");

            }
            catch(Exception ex)
            {
                lblMessage.Text = ex.Message;
                DisposeResources(ref connectCmd, ref cmd,ref dataAdapter, ref ds);
                return;
            }

            if (ds.Tables["Customers"].Rows.Count == 1)
            {
                _default.customerId = Convert.ToInt32(ds.Tables["Customers"].Rows[0]["Customer#"]);
                txtCustName.Text = ds.Tables["Customers"].Rows[0]["CustomerName"].ToString();
                txtCustAddress.Text = ds.Tables["Customers"].Rows[0]["Address"].ToString();
                txtCustCity.Text = ds.Tables["Customers"].Rows[0]["City"].ToString();
                txtProvince.Text = ds.Tables["Customers"].Rows[0]["Province"].ToString();
                txtPostCode.Text = ds.Tables["Customers"].Rows[0]["PostalCode"].ToString();
                txtEmail.Text = ds.Tables["Customers"].Rows[0]["Email"].ToString();
                txtCustPhone.Text = ds.Tables["Customers"].Rows[0]["PhoneNumber"].ToString();
            }
            else
            {
                lblMessage.Text = "Customer Not Found";
            }
            DisposeResources(ref connectCmd, ref cmd, ref dataAdapter, ref ds);
        }
        protected void btnNew_Click(object sender, EventArgs e)
        {
            resetTextField();
            hide();
        }

        private void displayMessage()
        {
            lblMessage.Visible = true;
        }

        private void hide()
        {
            lblMessage.Visible = false;
        }

        protected void btnVerify_Click(object sender, EventArgs e)
        {
            deleteCust();
        }

        private static void DisposeResources(ref SqlConnection connectCmd, ref SqlCommand cmd)
        {
            if(connectCmd != null)
            {
                connectCmd.Dispose();
            }
            if(cmd != null)
            {
                cmd.Dispose();
            }
        }

        private static void DisposeResources(ref SqlConnection connectCmd, ref SqlCommand cmd, ref SqlDataAdapter dataAdapter, ref DataSet ds)
        {
            if (connectCmd != null)
            {
                connectCmd.Dispose();
            }
            if (cmd != null)
            {
                cmd.Dispose();
            }

            if (dataAdapter != null)
            {
                dataAdapter.Dispose();
            }
            if (ds != null)
            {
                ds.Dispose();
            }

        }
    }
}