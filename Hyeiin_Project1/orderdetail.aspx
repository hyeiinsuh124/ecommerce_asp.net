﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="orderdetail.aspx.cs" Inherits="Hyeiin_Project1.orderdetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Styles/orderDetails.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great&display=swap" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblOrderDetail" CssClass="Labels" style="font-size: 50px; top: 24px; left: 360px;" runat="server" Text="Order Detail"></asp:Label>

        <asp:Table ID="tblDetail" CssClass="CellStyle" runat="server"></asp:Table>
        <asp:Label ID="lblTotal" CssClass="Labels" runat="server" Text="0.00"></asp:Label>

        <asp:CheckBox ID="chkAddMailing" CssClass="Checkboxes" AutoPostback="false" Text="Add me to the Mailing List" runat="server" />

        <asp:Button ID="BtnToBack" CssClass="Buttons" style="top: 177px; left: 788px; width: 85px" runat="server" Text="Shop More" OnClick="BtnToBack_Click" />
        <asp:Button ID="BtnToPay" CssClass="Buttons" style="top: 177px; left: 673px; width: 85px; right: 360px;"  runat="server" Text="Pay Now" />
    </form>
</body>
</html>
