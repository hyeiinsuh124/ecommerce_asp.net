﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="catalog.aspx.cs" Inherits="Hyeiin_Project1.catalog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Be Healthy Catalog</title>
    <link rel="stylesheet" href="Styles/catalog.css" />
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great&display=swap" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblCatalog" style="font-size: 50px;" runat="server" Text="Be Healthy Product Catalog"></asp:Label>

        <asp:Table ID="tblCatalog" CssClass="CellStyle" runat="server"></asp:Table>
        <asp:Button ID="btnTemplate" runat="server" Text="Button" Visible="False" OnClick="btnTemplate_Click" />

        <asp:Label ID="lblSelected" CssClass="Labels" runat="server" Text="Please select the product"></asp:Label>
    </form>
</body>
</html>
