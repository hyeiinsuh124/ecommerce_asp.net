﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="products.aspx.cs" Inherits="Hyeiin_Project1.products" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Be Healthy Products</title>
    <link rel="stylesheet" href="Styles/productStyle.css" />
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great&display=swap" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblProducts" CssClass="Labels" style="top:50px; font-size: 50px;" runat="server" Text="Our Products List"></asp:Label>

        <asp:Image ID="imgContainer" CssClass="Images" style="top: 148px; height:370px; width:255px" runat="server" />
       
        <asp:Label ID="lblProductId" CssClass="Labels" style="top:149px;" runat="server" Text="Product #"></asp:Label>
        <asp:TextBox ID="txtProductId" CssClass="TextBoxs" style="top:150px; width: 52px;" runat="server"></asp:TextBox>

        <asp:Label ID="lblProductCode" CssClass="Labels" style="top:200px;" runat="server" Text="Product Code"></asp:Label>
        <asp:TextBox ID="txtProductCode" CssClass="TextBoxs" style="top:200px; width: 95px;" runat="server"></asp:TextBox>

        <asp:Label ID="lblProductName" CssClass="Labels" style="top:250px;" runat="server" Text="Product Name"></asp:Label>
        <asp:TextBox ID="txtProductName" CssClass="TextBoxs" style="top: 250px; width: 179px;" runat="server"></asp:TextBox>

        <asp:Label ID="lblDescrip" CssClass="Labels" style="top: 300px;" runat="server" Text="Description"></asp:Label>
        <asp:TextBox ID="txtDescrip" CssClass="TextBoxs" style="top: 300px; width: 304px;" runat="server"></asp:TextBox>

        <asp:Label ID="lblPic" CssClass="Labels" style="top: 350px;" runat="server" Text="Picture Source"></asp:Label>
        <asp:TextBox ID="txtPic" CssClass="TextBoxs" style="top: 350px; width: 195px;" runat="server"></asp:TextBox>

        <asp:Label ID="lblQuantity" CssClass="Labels"  style="top: 400px;" runat="server" Text="Quantity On Hand"></asp:Label>
        <asp:TextBox ID="txtQuantity" CssClass="TextBoxs" style="top: 400px; width: 60px;" runat="server"></asp:TextBox>

        <asp:Label ID="lblPrice" CssClass="Labels" style="top: 450px;" runat="server" Text="Price"></asp:Label>
        <asp:TextBox ID="txtPrice" CssClass="TextBoxs" style="top: 450px; width: 98px;" runat="server"></asp:TextBox>

        <asp:Button ID="btnFind"  CssClass="Buttons" style="left: 447px" runat="server" Text="Search" OnClick="btnFind_Click" />
        <asp:Button ID="btnNew"  CssClass="Buttons" style="left: 600px; right: 911px;" runat="server" Text="New" OnClick="btnNew_Click" />
        <asp:Button ID="btnAdd"  CssClass="Buttons" style="left: 756px" runat="server" Text="Add" OnClick="btnAdd_Click" />
        <asp:Button ID="btnUpdate"  CssClass="Buttons" style="left: 907px" Enabled="false" runat="server" Text="Update" OnClick="btnUpdate_Click" />
        <asp:Button ID="btnDelete"  CssClass="Buttons" style="left: 1063px" Enabled="false" runat="server" Text="Delete" OnClick="btnDelete_Click" />
        <asp:Label ID="lblMessage" CssClass="Labels" style="top: 550px; left: 444px; bottom: 67px; color: #ffc0cb" Visible="false" runat="server" Text=""></asp:Label>
        <asp:Button ID="btnVerify"  CssClass="Buttons" style="left: 1062px; width: 178px;" Visible="false" runat="server" Text="Are you sure to delete?" OnClick="btnVerify_Click"/>

        <asp:DropDownList ID="dropProductPhoto" CssClass="DropDowns" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dropProductPhoto_SelectedIndexChanged"></asp:DropDownList>

    </form>
</body>
</html>
