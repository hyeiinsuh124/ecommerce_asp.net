﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cart.aspx.cs" Inherits="Hyeiin_Project1.cart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Your Cart</title>
    <link rel="stylesheet" href="Styles/cartStyle.css"/>
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great&display=swap" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblCartTitle" CssClass="Labels" style="font-size:50px; left:450px" runat="server" Text="Your Healthy Cart"></asp:Label>
        <asp:Table ID="tblCart" CssClass="CellStyle" runat="server"></asp:Table>
        <asp:Button ID="btnTemplateRemove" runat="server" Text="Button" style="visibility:hidden"/>
        <asp:Label ID="lblSelectedRow" CssClass="Labels" style="top: 126px;" runat="server" Text="You Selected"></asp:Label>
        <asp:Label ID="lblTotal" CssClass="Labels" runat="server" Text="0.00"></asp:Label>
        <asp:Button ID="btnRecalc" CssClass="Buttons" style="top: 169px; height: 29px" runat="server" Text="Recalculate" OnClick="btnRecalc_Click" />
        <asp:Button ID="btnCheckOut" CssClass="Buttons" style="top: 212px; height: 29px" runat="server" Text="Check Out" OnClick="btnCheckOut_Click" />
    </form>
</body>
</html>
